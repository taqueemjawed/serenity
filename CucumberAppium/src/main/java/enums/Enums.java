package enums;

public class Enums {

    public enum GlobalProperties {
        platform,
        device,
        deviceName,
        browser,
        apkName,
        testOn,
        packageName,
        activityName;
    }

    public enum Devices {
        EMULATOR,
        MOBILE;
    }

    public enum Platforms {
        ANDROID,
        IOS,
        DESKTOP;
    }

    public enum TestOn {
        APKFILE,
        APP,
        BROWSER;
    }


}
