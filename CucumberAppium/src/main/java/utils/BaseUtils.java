package utils;

import enums.Enums;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BaseUtils {

    WebDriver driver;
    AppiumDriverLocalService service;
    DesiredCapabilities caps;

    protected void projectLevelSetup() {
        setProperty();
        setDesiredCapability();
        intializeDriver();

    }


    protected void startServer() {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
    }

    protected void stopServer() {
        if (service.isRunning()) {
            System.out.println("Server is running");
        }
        service.stop();
    }

    private void setProperty() {
        Properties properties = new Properties();
        try {
            File propFile = FileUtils.getFile(ConstantUtils.pathGlobalProperties);
            if (propFile.exists()) {
                InputStream inputStream = new FileInputStream(propFile);
                properties.load(inputStream);
                for (String key : properties.stringPropertyNames()) {
                    if (System.getProperty(key) == null || System.getProperty(key).isEmpty())
                        System.setProperty(key, properties.getProperty(key));
                }
            }
        } catch (Exception e) {
            System.out.println("Exception happened");
            e.printStackTrace();
        }

    }

    private void setDesiredCapability() {

        String device = System.getProperty(Enums.GlobalProperties.device.toString());
        String deviceName = System.getProperty(Enums.GlobalProperties.deviceName.toString());
        String browser = System.getProperty(Enums.GlobalProperties.browser.toString());
        String testOn = System.getProperty(Enums.GlobalProperties.testOn.toString());
String packageName =System.getProperty(Enums.GlobalProperties.packageName.toString());
        String  activityName =System.getProperty(Enums.GlobalProperties.activityName.toString());
        String platform = System.getProperty(Enums.GlobalProperties.platform.toString());
        caps = new DesiredCapabilities();
        if (platform.equalsIgnoreCase(Enums.Platforms.ANDROID.toString())) {
            if (testOn.equalsIgnoreCase(Enums.TestOn.APKFILE.toString())) {
                if (device.equalsIgnoreCase(Enums.Devices.EMULATOR.toString())) {
                    caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                } else if (device.equalsIgnoreCase(Enums.Devices.MOBILE.toString())) {
                    caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
                }
                String apkName = System.getProperty(Enums.GlobalProperties.apkName.toString());
                File apk = new File(ConstantUtils.pathapk, apkName);
                caps.setCapability(MobileCapabilityType.APP, apk.getAbsolutePath());
                caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
            } else if (testOn.equalsIgnoreCase(Enums.TestOn.APP.toString())) {
                if (device.equalsIgnoreCase(Enums.Devices.EMULATOR.toString())) {
                    caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
                } else if (device.equalsIgnoreCase(Enums.Devices.MOBILE.toString())) {
                    caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
                }
                caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,packageName);
                caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,activityName);
            } else
                caps.setCapability(MobileCapabilityType.BROWSER_NAME, browser);
        } else
            Assert.fail("Only Supporting Android platform now");

        System.out.println("exiting the Set desired capability method");

    }

    private void intializeDriver() {
        String platform = System.getProperty(Enums.GlobalProperties.platform.toString());
        if (platform.equalsIgnoreCase(Enums.Platforms.ANDROID.toString())) {
            try {
                driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
            } catch (MalformedURLException e) {
                System.out.println("Exception happened");
                e.printStackTrace();
            }
        } else
            Assert.fail("Only Supporting Android platform now");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        System.out.println("initialised the driver");
    }


}
