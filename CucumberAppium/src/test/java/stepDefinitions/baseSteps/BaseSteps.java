package stepDefinitions.baseSteps;


import cucumber.api.java.Before;
import utils.BaseUtils;


public class BaseSteps extends BaseUtils {


    @Before
    public void runBeforeScenario() {
        System.out.println("Inside the runBeforeScenario");
        projectLevelSetup();
    }


}
