package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Steps;
import pageModels.googlePages.GoogleHome;

public class googleSteps {


    GoogleHome googleHome;


    @Given("^navigate to  Google home page$")
    public void navigate_to_Google_home_page() {
        // Write code here that turns the phrase above into concrete actions
        googleHome.open();

    }

    @When("^search for the text \"([^\"]*)\"$")
    public void search_for_the_text(String arg1) {
        // Write code here that turns the phrase above into concrete actions
        googleHome.printAlltheSuggestions(arg1);
    }



}
