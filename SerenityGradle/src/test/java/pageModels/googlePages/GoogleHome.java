package pageModels.googlePages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;

import java.util.List;

@DefaultUrl("https://www.google.com/")
public class GoogleHome extends PageObject {

    @FindBy(name="q")
    private WebElementFacade inputSearchBox;
@Step
    public void printAlltheSuggestions(String text){
        inputSearchBox.type(text);
        List<WebElementFacade> suggestions =  inputSearchBox.then(By.xpath("../../../../..")).thenFindAll(By.xpath("//ul[@role='listbox']//li[@role='presentation']"));
        for (WebElementFacade suggestion:suggestions) {
            System.out.println(suggestion.toString());
            System.out.println("text is "+suggestion.getText());

        }
    }


}
