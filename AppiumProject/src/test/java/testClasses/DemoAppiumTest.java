package testClasses;


import okio.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import pages.apiDemoPages.APIDemo;
import utils.BaseTestClass;
import utils.ProjectConstants;

import java.io.File;
import java.time.Duration;
import java.time.Duration.*;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;

public class DemoAppiumTest extends BaseTestClass {

    @Test
    public void testDemo() {
        System.out.println("Hello There.....I am so upset today");
        apiDemo.optionPreference.click();
        apiDemo.childPreferenceDependencies.click();
        apiDemo.checkBoxWifi.click();
        apiDemo.textWifiSettings.click();
        apiDemo.inputWifiSettings.sendKeys("JawedWiFi");
        apiDemo.buttonOK.click();
    }

    @Test
    public void testLongPress() {
        System.out.println("Testing a long press");
        apiDemo.optionViews.click();
        apiDemo.viewExpandableLists.click();
//        apiDemo.viewCustomAdapter.click();
        WebElement viewCustomAdapter = driver.findElement(By.xpath("//android.widget.TextView[@text='1. Custom Adapter']"));
        touchActions().tap(tapOptions().withElement(element(viewCustomAdapter))).perform();
        WebElement viewPeopleNames = driver.findElement(By.xpath("//android.widget.TextView[@text='People Names']"));
        touchActions().longPress(longPressOptions().withElement(element(viewPeopleNames)).withDuration(Duration.ofSeconds(5))).release().perform();

    }

    @Test
    public void testSwipeActions() {
        System.out.println("Testing a long press");
        apiDemo.optionViews.click();
        apiDemo.textDateWidgets.click();
        apiDemo.optionInline.click();
        apiDemo.hour9.click();
        WebElement minute15 = driver.findElement(By.xpath("//*[@index='3']"));
        WebElement minute45 = driver.findElement(By.xpath("//*[@content-desc='45']"));
        touchActions().longPress(longPressOptions().withElement(element(minute15)).withDuration(Duration.ofSeconds(2))).moveTo(element(minute45)).release().perform();

    }


    @Test
    public void testScrolling() {
        apiDemo.optionViews.click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"WebView\")); ");
    }

    @Test
    public void dragAndDrop() {
        apiDemo.optionViews.click();
        apiDemo.viewDragandDrop.click();
        WebElement from = (WebElement) driver.findElementsByClassName("android.view.View").get(0);
        WebElement to = (WebElement) driver.findElementsByClassName("android.view.View").get(1);
        dragAndDrop(from, to);
    }

    @Test
    public void runInBrowser() {
        driver.get("https://www.google.com/");
    }


    @Test
    public void checktheEmulatorPath() {

        String path = ProjectConstants.pathEmulatorStartBatch;
        try {
            Runtime.getRuntime().exec(path);

        } catch (Exception E) {
            E.printStackTrace();
        }

    }

}
