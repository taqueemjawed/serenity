package utils;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import pages.apiDemoPages.APIDemo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static java.time.Duration.ofSeconds;

import java.util.concurrent.TimeUnit;

public class BaseTestClass {

    protected AndroidDriver driver;
    DesiredCapabilities caps;


    protected APIDemo apiDemo = new APIDemo();

    @BeforeTest
    public void beforeSuite() {
        System.out.println("Inside before test");
        startServer();
//        setDesiredCapability("emulator");
        setDesiredCapability("emulator", "Chrome");
        intializeDriver();
        initializePages();
    }

    @AfterTest
    public void afterTest() {
        stopServer();
    }

    private void setDesiredCapability(String device, String Browser) {

        caps = new DesiredCapabilities();
        if (device.equals("emulator"))
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "JawedEmulator");
        else if (device.equals("real"))
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
        else
            Assert.fail("Please select a valid device or emulator");
        caps.setCapability(MobileCapabilityType.BROWSER_NAME, Browser);
//        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
        System.out.println("exiting the Set desired capability method");

    }


    private void setDesiredCapability(String device) {
        File apkPath = new File("src/main/resources/apks");
        File demoApk = new File(apkPath, "ApiDemos-debug.apk");
        caps = new DesiredCapabilities();
        if (device.equals("emulator"))
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "JawedEmulator");
        else if (device.equals("real"))
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
        else
            Assert.fail("Please select a valid device or emulator");
        caps.setCapability(MobileCapabilityType.APP, demoApk.getAbsolutePath());
        caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
        System.out.println("exiting the Set desired capability method");

    }

    private void intializeDriver() {
//        if (Platform.getCurrent().is(Platform.ANDROID)) {
        try {
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        } catch (MalformedURLException e) {
            System.out.println("Exception happened");
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        System.out.println("initialised the driver");
    }

    private void initializePages() {
        PageFactory.initElements(driver, apiDemo);
    }

    private static SoftAssert softAssert;

    protected TouchAction touchActions() {
        TouchAction action = new TouchAction(driver);
        return action;
    }


    protected void longPress(WebElement element) {
        touchActions().longPress(longPressOptions().withElement(element(element)).withDuration(ofSeconds(2))).perform();
    }

    protected void dragAndDrop(WebElement elementfrom, WebElement elementTo) {
        touchActions().longPress(element(elementfrom))
                .moveTo(element(elementTo)).release().perform();
    }

    AppiumDriverLocalService service;

    protected void startServer() {
        service = AppiumDriverLocalService.buildDefaultService();
        service.start();
    }

    protected void stopServer() {
        if (service.isRunning()) {
            System.out.println("Server is running");
        }
        service.stop();
    }


}
