package utils;

public class ProjectConstants {

    public static String userDir = System.getProperty("user.dir");

    public static String pathDemoAPK = "../apks/ApiDemos-debug.apk";
    public static String pathEmulatorStartBatch = userDir + "\\src\\main\\resources\\BatchFiles\\StartEmulator.bat";

}
