package pages.apiDemoPages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.BaseTestClass;

import java.util.List;

public class APIDemo {

    @FindBy(xpath = "//android.widget.TextView[@text='Preference']")
    public WebElement optionPreference;

    @FindBy(xpath = "//android.widget.TextView[@text='3. Preference dependencies']")
    public WebElement childPreferenceDependencies;

    @FindBy(xpath = "//android.widget.CheckBox[@resource-id='android:id/checkbox']")
    public WebElement checkBoxWifi;

    @FindBy(xpath = "//android.widget.LinearLayout[@index='2']")
    public WebElement textWifiSettings;

    @FindBy(xpath = "//android.widget.EditText[@resource-id='android:id/edit']")
    public WebElement inputWifiSettings;

    @FindBy(xpath = "//android.widget.Button[@resource-id='android:id/button1']")
    public WebElement buttonOK;
    //  "attribute("value")"
//    @AndroidFindBy(uiAutomator = "resource-id('android:id/button1')")
//    public WebElement buttonOK;

//    @AndroidFindBy(uiAutomator = "new UiSelector().resourceId(\"android:id/text1\")")
//    public WebElement androidUIAutomatorViews;

    //    @AndroidFindBy(uiAutomator = "new UiSelector().resource-Id(\"android:id/button1\")")
//    public List<WebElement> buttonOK;
    @FindBy(xpath = "//android.widget.TextView[@text='Views']")
    public WebElement optionViews;

    @FindBy(xpath = "//android.widget.TextView[@text='Expandable Lists']")
    public WebElement viewExpandableLists;

    @FindBy(xpath = "//android.widget.TextView[@text='Drag and Drop']")
    public WebElement viewDragandDrop;

    @FindBy(xpath = "//android.widget.TextView[@text='1. Custom Adapter']")
    public WebElement viewCustomAdapter;

    @FindBy(xpath = "//android.widget.TextView[@text='People Names']")
    public WebElement viewPeopleNames;

    @FindBy(xpath = "//android.widget.TextView[@text='Date Widgets']")
    public WebElement textDateWidgets;

    @FindBy(xpath = "//android.widget.TextView[@text='2. Inline']")
    public WebElement optionInline;


    @FindBy(xpath = "//*[@index='8']")
    public WebElement hour9;

    @FindBy(xpath = "//*[@index='3']")
    public WebElement minute15;



}
