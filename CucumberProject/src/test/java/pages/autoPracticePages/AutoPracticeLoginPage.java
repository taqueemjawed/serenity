package pages.autoPracticePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;


public class AutoPracticeLoginPage extends PageObject {

	@FindBy(xpath = "//input[@id='email']")
	private WebElement	inputEmail;

	@FindBy(xpath = "//input[@id='passwd']")
	private WebElement	inputPassword;

	@FindBy(xpath = "//button[@id='SubmitLogin']//span[1]")
	private WebElement	buttonSignIn;

	/**
	 * Set default value to Email Address Text field.
	 *
	 * @return the AutoPracticeLoginPage class instance.
	 */
	@Step
	public AutoPracticeLoginPage setEmailAddressTextField(String emailAddress) {

		inputEmail.sendKeys(emailAddress);
		return this;
	}
	@Step
	public AutoPracticeLoginPage setPasswordTextField(String password) {

		inputPassword.sendKeys(password);
		return this;
	}

	/**
	 * Click on Sign In Button.
	 *
	 * @return the AutoPracticeLoginPage class instance.
	 */
	public AutoPracticeLoginPage clickSignInButton() {

		buttonSignIn.click();
		return this;
	}
	
}
