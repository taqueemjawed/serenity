package pages.autoPracticePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;


@DefaultUrl("http://automationpractice.com/index.php")
public class AutoPracticeHomePage extends PageObject{

	
	@FindBy(css = "a.login")
	@CacheLookup
	private WebElement linkSignIn;

	/**
	 * Click on Sign In Link.
	 *
	 * @return the AutoPracticeHomePage class instance.
	 */
	@Step
	public AutoPracticeHomePage clickSignInLink() {

		linkSignIn.click();
		return this;
	}
}
