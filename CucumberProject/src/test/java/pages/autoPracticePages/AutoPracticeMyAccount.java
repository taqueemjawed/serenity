package pages.autoPracticePages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

public class AutoPracticeMyAccount extends PageObject {

    @FindBy(css = ".logout")
    @CacheLookup
    private WebElement linkSignOut;


    @FindBy(css = "#header_logo")
    @CacheLookup
    private WebElement imageLogo;

    @FindBy(css = "#search_query_top")
    @CacheLookup
    private WebElement inputSearchBox;

    @FindBy(css = ".shopping_cart")
    @CacheLookup
    private WebElement linkCart;

    /**
     * Click on Sign In Link.
     *
     * @return the AutoPracticeHomePage class instance.
     */
    @Step
    public AutoPracticeMyAccount clickSignInLink() {

        linkSignOut.click();
        return this;
    }

    @Step
    public boolean isLinkSignOutDisplayed() {
        boolean isLinkSignOutDisplayed = false;
        try {
            linkSignOut.isDisplayed();
            isLinkSignOutDisplayed = true;
        } catch (Exception e) {
            e.printStackTrace();

        }

        return isLinkSignOutDisplayed;


    }

    @Step
    public AutoPracticeMyAccount isDisplayedImageLogo() {
        imageLogo.isDisplayed();
        return this;
    }


    @Step
    public AutoPracticeMyAccount isDisplayedInputSearchBox() {
        inputSearchBox.isDisplayed();
        return this;
    }

    @Step
    public AutoPracticeMyAccount isDisplayedLinkCart() {
        linkCart.isDisplayed();
        return this;
    }
}
