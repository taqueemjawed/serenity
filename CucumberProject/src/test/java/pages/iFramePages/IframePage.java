package pages.iFramePages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://the-internet.herokuapp.com/iframe")
public class IframePage extends PageObject {


    @FindBy(id = "mce_0_ifr")
    @CacheLookup
    private WebElement iFrame;

    @FindBy(id = "tinymce")
    @CacheLookup
    private WebElement body;

    @FindBy(id = "mceu_15-open")
    @CacheLookup
    private WebElement buttonFile;



    @Step
    public IframePage clickOnButtonFile() {
        switchToDefault();
        clickOn(buttonFile);
        return this;
    }



    @Step
    public IframePage switchToFrame() {
        getDriver().switchTo().frame(iFrame);
        return this;
    }

    @Step
    public IframePage switchToDefault() {
        getDriver().switchTo().defaultContent();
        return this;
    }

    @Step
    public IframePage setTextinBody(String text) {
    body.sendKeys(text);
        return this;
    }
}

