package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.OpenUrl;
import org.junit.Assert;
import pages.iFramePages.IframePage;

public class GenericSteps {

IframePage iframePage;
    @Given("^User enter the url$")
    public void user_enter_the_url() {
        // Write code here that turns the phrase above into concrete actions
        iframePage.open();
    }

    @When("^user switches the frame$")
    public void user_switches_the_frame() {
        // Write code here that turns the phrase above into concrete actions
        iframePage.switchToFrame();
    }


    @When("^enters some text$")
    public void enters_some_text() {
        // Write code here that turns the phrase above into concrete actions
        iframePage.setTextinBody("Hello");
    }

    @Then("^user is able to see the text inside the frame$")
    public void user_is_able_to_see_the_text_inside_the_frame() {
        // Write code here that turns the phrase above into concrete actions
        Assert.assertEquals(iframePage.getTitle(),"The Internet");
    }

    @Then("^click on the file$")
    public void click_on_the_file() {
        // Write code here that turns the phrase above into concrete actions
        iframePage.clickOnButtonFile();
    }

    @Given("^navigate to \"([^\"]*)\"$")
    public void navigate_to(String url) {
        // Write code here that turns the phrase above into concrete actions
        iframePage.openUrl(url);
    }

}
