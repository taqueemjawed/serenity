package stepDefinitions;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import net.thucydides.core.annotations.Steps;
import pages.autoPracticePages.AutoPracticeHomePage;
import pages.autoPracticePages.AutoPracticeLoginPage;
import pages.autoPracticePages.AutoPracticeMyAccount;

public class AutoPracticeSteps {

	AutoPracticeHomePage autoPracticeHomePage;
@Steps
	AutoPracticeLoginPage autoPracticeLoginPage;
@Steps
	AutoPracticeMyAccount autoPracticeMyAccount;
	
	@Given("^User hit the url$")
	public void user_hit_the_url() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeHomePage.open();
		
	}

	@When("^user clicks on Signin link$")
	public void user_clicks_on_Signin_link() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeHomePage.clickSignInLink();
	}

	@When("^enters userName and Password$")
	public void enters_userName_and_Password() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeLoginPage.setEmailAddressTextField("taqueem_toolsqa@outlook.com").setPasswordTextField("Learn123$");
		
	}

	@When("^clicks on signin Button$")
	public void clicks_on_signin_Button() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeLoginPage.clickSignInButton();
	}

	@SuppressWarnings("deprecation")
	@Then("^user is able to see the homepage$")
	public void user_is_able_to_see_the_homepage() {
	    // Write code here that turns the phrase above into concrete actions
	Assert.assertEquals("My account - My Store", autoPracticeMyAccount.getTitle());
	}
	
	
	@Then("^user able to see the logo$")
	public void user_able_to_see_the_logo() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeMyAccount.isDisplayedImageLogo();
	    
	}

	@Then("^user able to see the Search box$")
	public void user_able_to_see_the_Search_box() {
	    // Write code here that turns the phrase above into concrete actions
		autoPracticeMyAccount.isDisplayedInputSearchBox();
	}

	@Then("^user able to see the Cart$")
	public void user_able_to_see_the_Cart() {
	    // Write code here that turns the phrase above into concrete actions
	   autoPracticeMyAccount.isDisplayedLinkCart();
	}


	@Given("^User is Logged in$")
	public void user_is_Logged_in() {
		// Write code here that turns the phrase above into concrete actions
		if(!autoPracticeMyAccount.isLinkSignOutDisplayed()) {

	//	if(autoPracticeHomePage.getDriver()==null){
			autoPracticeHomePage.open();
			autoPracticeHomePage.clickSignInLink();
			autoPracticeLoginPage.setEmailAddressTextField("taqueem_toolsqa@outlook.com")
					.setPasswordTextField("Learn123$")
					.clickSignInButton();
			autoPracticeMyAccount.isDisplayedImageLogo();
		}else{
			System.out.println("Else comdition");
		}


	}
	
}
