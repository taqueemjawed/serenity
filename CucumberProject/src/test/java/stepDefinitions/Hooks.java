package stepDefinitions;

import cucumber.api.java.Before;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.webdriver.SerenityWebdriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class Hooks {


    @Before
    public void setUpDriverOptions(){
        System.out.println("Inside setUpDriverOptions" );
     /*   InternetExplorerOptions ieOptions = new InternetExplorerOptions();
        ieOptions.introduceFlakinessByIgnoringSecurityDomains();
        System.setProperty("webdriver.ie.driver","src/test/resources/drivers/IEDriverServer.exe");
        WebDriver driver = new InternetExplorerDriver(ieOptions);
        Serenity.getWebdriverManager().setCurrentDriver(driver);*/
       // System.out.println(Serenity.getWebdriverManager().getCurrentDriver().toString());
// this befeore hooks work even before the properties are loaded

         FirefoxProfile myProfile = new FirefoxProfile();
         myProfile.setPreference("network.proxy.socks_port", 9999);
         myProfile.setAlwaysLoadNoFocusLib(true);
         Serenity.useFirefoxProfile(myProfile);


    }

}
