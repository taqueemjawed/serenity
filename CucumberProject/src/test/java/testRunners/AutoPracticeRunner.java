package testRunners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/autoPractice/PankuTest.feature",
glue= {"stepDefinitions"} )
public class AutoPracticeRunner {

}
